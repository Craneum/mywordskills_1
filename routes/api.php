<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('auth', function (Request $request) {

	$login=$request->login;//присваиваем переменной login post параметр login
	$password=$request->password;//присваиваем переменной password post параметр password
	   if ((Auth::attempt(['login' => $login, 'password' => $password],true)) and ($login=='admin')) {
		 $token=App\User::whereLogin($request->login)->first()->remember_token;//$this->remember_token
		 $body =array ('status'=>true ,'token'=>$token);
		 $arr = array(  'status code' => 200, 
		   'status text' => 'Successful authorization',
		   'body'=>	$body
		    );
		   return json_encode($arr);
		   // Аутентификация прошла успешно
	   } else {
		  $body =array ('status'=>false ,'message'=>'Invalid authorization data');
		 $arr = array(  'status code' => 401, 
		   'status text' => 'Invalid authorization data',
		   'body'=>	$body
		    );
		   return json_encode($arr);
	   }
});
Route::group(['prefix'=>'posts'], function(){
    Route::get('/{post}/comments/', 'ComentController@index');
    Route::get('/{post}/comments/{comment}', 'ComentController@show');
    Route::delete('/{post}/comments/{comment}', 'ComentController@destroy');
    Route::post('/{post}/comments/{comment}', 'ComentController@update');
    Route::post('/{post}/comments', 'ComentController@store');
  });
  Route::get('/posts', 'PostController@index');
  Route::get('/posts/{post}', 'PostController@show');
  Route::delete('/posts/{post}', 'PostController@destroy');
  Route::post('/posts/{post}', 'PostController@update');
  Route::post('/posts', 'PostController@store');
  Route::get('/posts/tag/{tagname}', 'PostController@search');