<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Post;
use Illuminate\Support\Facades\Storage;
use  App\Http\Resources\Post\PostCollection;
use  \App\Http\Resources\Post\PostResource;

////
class PostController extends Controller
{
    private $image_ext = ['jpg','JPG', 'jpeg','JPEG', 'png','PNG'];
	  private function getType($ext)
  {
    if (in_array($ext, $this->image_ext)) {
      return 'image';
    }

    if (in_array($ext, $this->audio_ext)) {
      return 'audio';
    }

    if (in_array($ext, $this->video_ext)) {
      return 'video';
    }

    if (in_array($ext, $this->document_ext)) {
      return 'document';
    }
  }

  private function allExtensions()
  {
    return array_merge($this->image_ext, $this->audio_ext, $this->video_ext, $this->document_ext);
  }

  
  private function getUserDir()
  {
	  
    return 'api/post_images';
  }

    public function store(Request $request)
    {
        $header=explode('Bearer ',$request->header('Authorization')); //запрос оброщается к методу bearer
		$user=User::whereRemember_token($header[1])->first();//return $header[1]; //возрошает имя пользователя
		//return $request;
		if (empty($user)){ //ветление
		 $body =array ('message'=>'Unauthorized'); //массив значение body
		 $arr = array(  'status code' => 401, //массив значение arr
		   'status text' => 'Unauthorized', //продолжение значения array
		   'body'=>	$body//продолжение значения array
		    );//продолжение значения array
		 return json_encode($arr); //возращает значение json
		
		} 
		else {// или
			//$request->image;
			    $param=0; //присваивает значению param=0
				$file = $request->file('image');//загружает на сервер значение
				$ext = $file->getClientOriginalExtension();//возвращает расширение файла, которое передал пользователь в названии файла.
				$type = $this->getType($ext);//возвращает ext
				$name=mt_rand(0, 10000);//Генерация случайного значения 
				$filename=$name. '.' . $ext; //
				$destination=$this->getUserDir(); //Возвращает домашний каталог текущего пользователя.
				//
				$param=filesize($file);//Возвращает размер файла
				
					if ($param>1048576) //Storage::putFileAs('/public/' . $this->getUserDir() . '/' , $file,$name  . '.' . $ext)// ветление 
						{
							$param=($param/1024)/1024; //возвращает разер файла в определеных рамках 
						
						$mes[]='Field Image:Unsupported file size(upload '.$param.' MB)';//присваивает переменной значение 
						$body =array ('status'=>'false','message'=>$mes);//отоброжает значение 
								$arr = array(  'status code' => 400,  //отоброжает зчание переменной 
								'status text' => 'Creating error', // текст вывода
								'body'=>	$body// выводит значение body 
									);
								return json_encode($arr);	//возращает значение json
						
						}
						else{ //иначе(условие не выполняется)
						
						$check=isset ($request->title) && isset ($request->anons) && isset($request->text);//вывод значение 
						if (!$check){ //условие(если выполняется)
						$mes[]='';//вывод значение
								//$keys=array('title','message');
								if (!isset($request->title)){ //(ветление);
									$mes[]='Field Title:null';//выводит значение
									}
								if (!isset($request->anons)){//(ветление);
									$mes[]='Field Anons:null';//выводит значение
									}
								if (!isset($request->text)){//(ветление);
									$mes[]='Field Text:null';//выводит значение
									}
								if (!isset($request->tags)){$mes[]='Field Tags:null';}//(ветление);
								
								$body =array ('status'=>'false','message'=>$mes);//отоброжает значение переменной
								$arr = array(  'status code' => 400, //отоброжает зачение переменной
								'status text' => 'Creating error',//выводит текст 
								'body'=>	$body //выводит текст и переменнюу
									);
								return json_encode($arr); //возрощает значение json
						}
						
								
								
							
						
							else//иначе
							{
						
								$find=Post::whereTitle($request->title)->first();
									if (!empty($find)){//ветление
										$mes[]='';//вывод значения
										$mes[]='Field Title:Data already exist';//вывод знаения
										$body =array ('status'=>'false','message'=>$mes);//вывод значения(присвоение)
										$arr = array(  'status code' => 400, //вывод значения 
											'status text' => 'Creating error', //вывод текста
											'body'=>	$body //вывод текста и переменной 
										);
									return json_encode($arr); //возвращает знчения json
									}
									else{//Иначе
											try { //если значение try
												 $file->move($destination, $filename); //присвоение значения move
												 $adr=$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']."/../../../".$destination."/".$filename;
													$post=new Post();
													$post->title=$request->title;
													$post->anons=$request->anons;
													$post->text=$request->text;
													$post->tags=$request->tags;
													$post->image=$adr;
													$post->save();
													
													$body =array ('status'=>'true','post_id'=>$post->id);
													$arr = array(  'status code' => 201, 
													'status text' => 'Successful creation',
													'body'=>	$body
														);
													return json_encode($arr);	
													
												}
											catch (Exception $ex) {
											//Выводим сообщение об исключении.
											echo $ex->getMessage();
												}		
										}
						 				 	
						
							}
						
						
						
						
						}
   
			
			
		}

    }//
    public function update(Request $request, Post $post)
    {
        $header=explode('Bearer ',$request->header('Authorization'));
		$user=User::whereRemember_token($header[1])->first();//return $header[1];
		
		if (empty($user)){
		 $body =array ('message'=>'Unauthorized');
		 $arr = array(  'status code' => 401, 
		   'status text' => 'Unauthorized',
		   'body'=>	$body
		    );
		 return json_encode($arr);
		
		} 
		else {
			 $param=0;
			 $adr='';
				if (isset($request->image)){
						$file = $request->file('image');
						$ext = $file->getClientOriginalExtension() ;
						$type = $this->getType($ext);
						$name=mt_rand(0, 10000);
						$filename=$name. '.' . $ext;
						$destination=$this->getUserDir();
						
						$param=filesize($file);
				}
				
				
					if ($param>1048576) //Storage::putFileAs('/public/' . $this->getUserDir() . '/' , $file,$name  . '.' . $ext)
						{
							$param=($param/1024)/1024;
						
						$mes[]='Field Image:Unsupported file size(upload '.$param.' MB)';
						$body =array ('status'=>'false','message'=>$mes);
								$arr = array(  'status code' => 400, 
								'status text' => 'Update error',
								'body'=>	$body
									);
								return json_encode($arr);	
						
						}
						else{
							$id=$post->id;
							
							$update_post = Post::find($id);
							
							
							
							$arr=array();
							
							if (isset($update_post)) {
								
								if ($request->title!==$update_post->title){
									$arr['title']=$request->title;
									}
								if ($request->anons!==$update_post->anons){
									$arr['anons']=$request->anons;
									}
								if ($request->text!==$update_post->text){
									$arr['text']=$request->text;
									}
								if ($request->tags!==$update_post->tags){
									$arr['tags']=$request->tags;
									}
								if (isset($request->image)){
									if ($adr!==$update_post->image){
									$adr=$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']."/../../../".$destination."/".$filename;
									$file->move($destination, $filename);
									$arr['image']=$adr;
									}	
								}	
								
								foreach ($arr as $mainKey=>$item) {
									if ($arr[$mainKey]==null){unset($arr[$mainKey]);}
								}
								
								$update_post->update($arr); 
								$myDateTime = date_create_from_format('Y-m-d H:i:s', $update_post->created_at);
								$newDateString = date_format($myDateTime,"H:i d.m.Y");
								
								
								$body =array ('status'=>'true','post'=>array(
																			'title'=>$update_post->title,
																			'datatime'=>$newDateString ,
																			'anons'=>$update_post->anons,
																			'text'=>$update_post->text,
																			'tags'=> preg_split("/[\s,]+/",$update_post->tags),
																			'image'=>$update_post->image///поменять data time
																			)
											);
													$arr = array(  'status code' => 201, 
													'status text' => 'Successful creation',
													'body'=>	$body
														);
													return json_encode($arr);	
							 }
						}
			
		}

	}
	public function destroy(Post $post)
	{
	$id=$post->id; //к переменой id присваиваем значение post id  
		$delete_post = Post::find($id)->first(); //
		$delete_post->delete(); // присваивает значения 
		$body =array ('status'=>'true'); //выводит значение true
													$arr = array(  'status code' => 201, //выдает надпись 
													'status text' => 'Successful delete',// выдает значение 
													'body'=>	$body
														);
													return json_encode($arr);		// возращает значечние json
    }
	public function index(Post $post)
	{
		$body= PostCollection::collection($post->all());// активирует контролер 
		$arr = array( 'status code'=>200, //выдает надпись 
		'status text'=>'List Posts', // выдает надпись 
		'data'=>$body); // выдает надпись 
		return json_encode($arr); //возращает json
	}
	Public function show(Post $post)
	{
		$body= new PostResource($post); // вызывает PostResource
			$arr = array( 'status code'=>200, // выводит надпись 
			'status text'=>'View Posts', //выводит надпись 
			'data'=>$body);
			return json_encode($arr); //возврощает json
	}

	
}
