<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\Resource;
class PostCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
          'title' => $this->title,
          'datatime' => date_format(date_create_from_format('Y-m-d H:i:s', $this->created_at),"H:i d.m.Y") ,
          'anons' => $this->anons,
          'text'=> $this->text,
          'tags'=>preg_split("/[\s,]+/",$this->tags),
          'image'=>$this->image
        ];
    }
}
