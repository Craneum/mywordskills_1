<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Coment;
class Post extends Model
{
    protected $fillable = ['title','anons','text','tags','image'];
    public function coments()
    {
        return $this->hasMany(Coment::class );
    }
}
